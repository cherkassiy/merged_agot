**AGOT MODS MERGED**
Basic:
- A Game of Thrones
- Pre-Doom bookmarks -- No history (yet), though, and the map isn't working properly

Larger overhauls:
- More Bloodlines
- Congenital Overhaul
- Trait Softcap Exterminated
- Faces Improvement **AND** Better Faces -- Frankish Andals Jonothor version, as backup
- Valyrian Mixed Ethnicities -- needs to be overhauled
- Thousand Islander Overhaul*

Events and systems:
- Naughty AGOT (events only)
- Seize Sword From Traitor
- Dragon Tamer Laws*
- Old Gods Reorganized*
- Storm God Rekindled*
- Winter Soldier
- Dueling Duelists

Map:
- Improved Borders*
- In heaven's map modifications*
- Trade Routes Revisisted
- YiTi Expanded*
- High Lordship of Bear Island*
- Isle of Faces
- Hornwood
- Codd
- Targaryen Islands

Misc:
- New Dragon Symbol + New Religion Icons + Valyrian Interface
- Fixed the religious reformations.
	
*Edited (probably heavily), reimplemented, or only integrated to an extend that it actually works with newer/other submods. Most mods have had some bugfixes.

**ADDED MYSELF**
- Reformable Storm God, Old Gods of the Mountain, Thenn
- Renamed Old God faiths for consistency: Old Gods Below the Wall, Old Gods Beyond the Wall, Old Gods of the Mountain
- Daenarys is pregnant in the months up to Drogo's death.
- Seduction without Seduction focus if you have the Seducer traits
- Removed the requirement of Charlemagne DLC needing to be disabled for most titles
- Pre-Doom bookmarks from the official submods added, but no history yet
	
**TO DO**
- Add flag on Daenarys that all children of her and Drogo are of the Dothraki-Valyrian ethnicity
- Centralize localisations
- See if the Valyrian Subcultures mod if anything good
- Fix histories for YiTi Expanded in Century of Blood bloodmark
- Fix histories for other provinces
- Add the wastelands from Improved Borders
- Beyond the Wall and Iron Islands copied over
- Make the dynastic ethnicities selectable in CC
- Duels without the War focus if you the Duelist trait
- Dynamic Seven Kingdoms: X Kingdoms depending on vassal king-tier titles
- Add Andal Ironborn culture: Ironman
- Fix the Wall's placement and the adjacent provinces


**TO MERGE**
- ? Colonizable Valyria
- ? Faces Modification + Better Garbs
- Knights of the Sword Tower
- Dragon of the East
