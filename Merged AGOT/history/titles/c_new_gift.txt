1.1.1={
	liege = "d_winterfell"
	de_jure_liege = d_northclans
	name = C_NORTH_ROAD
	effect = { location = { set_name = C_NORTH_ROAD } }
}
8059.1.1={
	liege = "d_the_gift"
	de_jure_liege = d_the_gift
	#holder = 121405 #Merlon Connington
	law = nightswatch_elective
	reset_name = yes
	effect = { location = { set_name = c_new_gift } }
}