# 625 - New Ghis

# County Title
title = c_newghis

# Settlements
max_settlements = 5

b_pyramidnewghis = castle
b_newghis = city
b_newghis_temple = temple

# Misc
culture = ghiscari
religion = harpy

# History
1.1.1 = {
	b_pyramidnewghis = ca_asoiaf_ghiscari_basevalue_1
	b_pyramidnewghis = ca_asoiaf_ghiscari_basevalue_2
	b_pyramidnewghis = ca_asoiaf_ghiscari_basevalue_3
	b_pyramidnewghis = ca_legionary_barracks
	b_pyramidnewghis = ca_culture_indian_1
	
	b_newghis = ct_asoiaf_ghiscari_basevalue_1
	b_newghis = ct_asoiaf_ghiscari_basevalue_2
	b_newghis = ct_asoiaf_ghiscari_basevalue_3
	b_newghis = ct_legionary_barracks
}
4000.1.1 = {
	b_newghisharbour = city
	b_pyramidnewghis_2 = castle
	
	b_pyramidnewghis = ca_asoiaf_ghiscari_basevalue_4
	b_pyramidnewghis = ca_asoiaf_ghiscari_basevalue_5	
		
	
	b_pyramidnewghis = ca_culture_indian_2
	b_pyramidnewghis = ca_culture_indian_3
	
	b_newghis = ct_asoiaf_ghiscari_basevalue_4
	b_newghis = ct_asoiaf_ghiscari_basevalue_5	
		
	b_pyramidnewghis = ca_asoiaf_essosshipyard
	
	b_newghisharbour = ct_asoiaf_ghiscari_basevalue_1
	b_newghisharbour = ct_asoiaf_ghiscari_basevalue_2
	b_newghisharbour = ct_asoiaf_ghiscari_basevalue_3
	b_newghisharbour = ct_legionary_barracks
	
	b_pyramidnewghis_2 = ca_asoiaf_ghiscari_basevalue_1
	b_pyramidnewghis_2 = ca_asoiaf_ghiscari_basevalue_2
	b_pyramidnewghis_2 = ca_asoiaf_ghiscari_basevalue_3
	b_pyramidnewghis_2 = ca_legionary_barracks

	trade_post = b_loraq # Hizdhar has connections there.
}