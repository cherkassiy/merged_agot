# 866  - Wu'an

# County Title
title = c_wuan

# Settlements
max_settlements = 6

b_wuan_castle1 = castle
b_wuan_city1 = city
b_wuan_temple = temple
b_wuan_city2 = city
b_wuan_castle2 = city
b_wuan_castle3 = castle

# Misc
culture = yi_ti
religion = yiti_gods

# History
1.1.1 = {

	b_wuan_castle1 = ca_asoiaf_yiti_basevalue_1
	b_wuan_castle1 = ca_asoiaf_yiti_basevalue_2
	b_wuan_castle1 = ca_asoiaf_yiti_basevalue_3

	b_wuan_city1 = ct_asoiaf_yiti_basevalue_1
	b_wuan_city1 = ct_asoiaf_yiti_basevalue_2
	b_wuan_city1 = ct_asoiaf_yiti_basevalue_3

	b_wuan_city2 = ct_asoiaf_yiti_basevalue_1
	b_wuan_city2 = ct_asoiaf_yiti_basevalue_2
	b_wuan_city2 = ct_asoiaf_yiti_basevalue_3

}
	