# 500 - Tall trees Town

# County Title
title = c_visenya

# Settlements
max_settlements = 2
b_visenya_1 = castle
b_visenya_2 = city

# Misc
culture = summer_islander
religion = summer_rel

terrain = jungle

#History
1.1.1 = {
	b_visenya_1 = ca_asoiaf_summerisland_basevalue_1
	
	b_visenya_2 = ct_asoiaf_summerisland_basevalue_1
	
}