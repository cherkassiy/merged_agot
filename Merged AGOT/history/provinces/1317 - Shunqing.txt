# 1220  - Shunqing

# County Title
title = c_shunqing

# Settlements
max_settlements = 4

b_shunqing_castle = castle
b_shunqing_city1 = city
b_shunqing_temple = temple
b_shunqing_city2 = city

# Misc
culture = yi_ti
religion = yiti_gods

# History
1.1.1 = {

	b_shunqing_castle = ca_asoiaf_yiti_basevalue_1
	b_shunqing_castle = ca_asoiaf_yiti_basevalue_2

	b_shunqing_city1 = ct_asoiaf_yiti_basevalue_1
	b_shunqing_city1 = ct_asoiaf_yiti_basevalue_2

}
	