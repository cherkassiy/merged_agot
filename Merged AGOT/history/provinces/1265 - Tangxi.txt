# 1168  - Tangxi

# County Title
title = c_tangxi

# Settlements
max_settlements = 4

b_tangxi_castle = castle
b_tangxi_city1 = city
b_tangxi_temple = temple
b_tangxi_city2 = city

# Misc
culture = yi_ti
religion = yiti_gods

# History
1.1.1 = {

	b_tangxi_castle = ca_asoiaf_yiti_basevalue_1
	b_tangxi_castle = ca_asoiaf_yiti_basevalue_2

	b_tangxi_city1 = ct_asoiaf_yiti_basevalue_1

}
	