# 1156  - Tingyun

# County Title
title = c_tingyun

# Settlements
max_settlements = 4

b_tingyun_castle = castle
b_tingyun_city1 = city
b_tingyun_temple = temple
b_tingyun_city2 = city

# Misc
culture = yi_ti
religion = yiti_gods

# History
1.1.1 = {

	b_tingyun_castle = ca_asoiaf_yiti_basevalue_1
	b_tingyun_castle = ca_asoiaf_yiti_basevalue_2

	b_tingyun_city1 = ct_asoiaf_yiti_basevalue_1
	b_tingyun_city1 = ct_asoiaf_yiti_basevalue_2

}
	