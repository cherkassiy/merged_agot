# 1217  - Shicheng

# County Title
title = c_shicheng

# Settlements
max_settlements = 4

b_shicheng_castle = castle
b_shicheng_city1 = city
b_shicheng_temple = temple
b_shicheng_city2 = city

# Misc
culture = yi_ti
religion = yiti_gods

# History
1.1.1 = {

	b_shicheng_castle = ca_asoiaf_yiti_basevalue_1
	b_shicheng_castle = ca_asoiaf_yiti_basevalue_2

	b_shicheng_city1 = ct_asoiaf_yiti_basevalue_1
	b_shicheng_city1 = ct_asoiaf_yiti_basevalue_2

}
	