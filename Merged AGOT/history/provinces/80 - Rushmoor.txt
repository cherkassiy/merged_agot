# 80 - Rushmoor

# County Title
title = c_rushmoor

# Settlements
max_settlements = 4

b_moorkeep = castle
b_moorville = city
b_rushmoor = castle

# Misc
culture = old_first_man
religion = old_gods

# History




1.1.1 = {

b_moorkeep = ca_asoiaf_river_basevalue_1

b_moorville = ct_asoiaf_river_basevalue_1
b_moorville = ct_asoiaf_river_basevalue_2

b_rushmoor = ca_asoiaf_river_basevalue_1
}

6700.1.1 = {
	culture = riverlander
}