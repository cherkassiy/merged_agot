# 234 - Sharp Point

# County Title
title = c_sharp_point

# Settlements
max_settlements = 3
b_sharp_point= castle
b_west_point = city
#b_sharp_point_sept = temple

#b_waycastle = castle
#b_valcrest = city


# Misc
culture = old_first_man
religion = old_gods

# History

1066.1.1 = {
	b_sharp_point= ca_asoiaf_crown_basevalue_1
	b_sharp_point= ca_asoiaf_crown_basevalue_2
	b_sharp_point= ca_asoiaf_crown_basevalue_3
	b_sharp_point= ca_asoiaf_smallshipyard
	
	b_west_point = ct_asoiaf_crown_basevalue_1
	b_west_point = ct_asoiaf_crown_basevalue_2
}
6700.1.1 = { 
	culture = stormlander
	religion = the_seven
}