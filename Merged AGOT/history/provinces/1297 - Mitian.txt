# 1200  - Mitian

# County Title
title = c_mitian

# Settlements
max_settlements = 4

b_mitian_castle = castle
b_mitian_city1 = city
b_mitian_temple = temple
b_mitian_city2 = city

# Misc
culture = yi_ti
religion = yiti_gods

# History
1.1.1 = {

	b_mitian_castle = ca_asoiaf_yiti_basevalue_1
	b_mitian_castle = ca_asoiaf_yiti_basevalue_2

	b_mitian_city1 = ct_asoiaf_yiti_basevalue_1
	b_mitian_city1 = ct_asoiaf_yiti_basevalue_2

}
	