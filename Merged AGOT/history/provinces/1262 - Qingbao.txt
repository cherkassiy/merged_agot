# 1165  - Qingbao

# County Title
title = c_qingbao

# Settlements
max_settlements = 4

b_qingbao_castle = castle
b_qingbao_city1 = city
b_qingbao_temple = temple
b_qingbao_city2 = city

# Misc
culture = yi_ti
religion = yiti_gods

# History
1.1.1 = {

	b_qingbao_castle = ca_asoiaf_yiti_basevalue_1
	b_qingbao_castle = ca_asoiaf_yiti_basevalue_2

	b_qingbao_city1 = ct_asoiaf_yiti_basevalue_1

}
	