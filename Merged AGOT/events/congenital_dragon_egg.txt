#Inform Egg revoked
character_event = {
	id = dragon_egg_steal.1
	desc = "EVTDESCdragon_egg_steal.1"

	is_triggered_only = yes
	show_from_from = yes

	option = {
		name = "EVTOPTAdragon_egg_steal.1"
		prestige = -50
	}
}