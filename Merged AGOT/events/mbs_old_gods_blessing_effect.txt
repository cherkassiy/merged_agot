namespace = mbs_old_gods_blessing_effect		

#mbs_old_gods_blessing_effect.1 - Mormont Snowbear
narrative_event = {
	id = mbs_old_gods_blessing_effect.1
	title = "EVTtitlembs_old_gods_blessing_effect.1"
	desc = "EVTDESCmbs_old_gods_blessing_effect.1"
	picture = "GFX_snowbear"
	
	is_triggered_only = yes

	option = {
		name = "EVTOPTAmbs_old_gods_blessing_effect.1" #get snowbear
		
		ai_chance = {
			factor = 2
		}
		
		prestige = 15
		
		ROOT = {
			opinion = {
				modifier = opinion_grateful
				who = FROM
				years = 5
			}
		}
			
		add_trait = snowbear
		set_character_flag = blessed_mormont_siblings
	}
}
#mbs_old_gods_blessing_effect.2 - Mormont Brownbear
narrative_event = {
	id = mbs_old_gods_blessing_effect.2
	title = "EVTtitlembs_old_gods_blessing_effect.2"
	desc = "EVTDESCmbs_old_gods_blessing_effect.2"
	picture = "GFX_brownbear"
	
	is_triggered_only = yes

	option = {
		name = "EVTOPTAmbs_old_gods_blessing_effect.2" #get brownbear
		
		ai_chance = {
			factor = 2
		}
		
		prestige = 15
		
		ROOT = {
			opinion = {
				modifier = opinion_grateful
				who = FROM
				years = 5
			}
		}
			
		add_trait = brownbear
		set_character_flag = blessed_mormont_siblings
	}
}
#mbs_old_gods_blessing_effect.3 - Blackwood Raven
narrative_event = {
	id = mbs_old_gods_blessing_effect.3
	title = "EVTtitlembs_old_gods_blessing_effect.3"
	desc = "EVTDESCmbs_old_gods_blessing_effect.3"
	picture = "GFX_crow"
	
	is_triggered_only = yes

	option = {
		name = "EVTOPTAmbs_old_gods_blessing_effect.3" #get raven
		
		ai_chance = {
			factor = 2
		}
		
		prestige = 15
		
		ROOT = {
			opinion = {
				modifier = opinion_grateful
				who = FROM
				years = 5
			}
		}
			
		add_trait = crow
		set_character_flag = blessed_blackwood_siblings
	}
}
#mbs_old_gods_blessing_effect.4 - Stark Direwolf
narrative_event = {
	id = mbs_old_gods_blessing_effect.4
	title = "EVTtitlembs_old_gods_blessing_effect.4"
	desc = "EVTDESCmbs_old_gods_blessing_effect.4"
	picture = "GFX_wolf"
	
	is_triggered_only = yes

	option = {
		name = "EVTOPTAmbs_old_gods_blessing_effect.4" #get raven
		
		ai_chance = {
			factor = 2
		}
		
		prestige = 15
		
		ROOT = {
			opinion = {
				modifier = opinion_grateful
				who = FROM
				years = 5
			}
		}
			
		add_trait = direwolf
		set_character_flag = blessed_stark_siblings
	}
}
#mbs_old_gods_blessing_effect.5 - Marsh Lizard-Lion
narrative_event = {
	id = mbs_old_gods_blessing_effect.5
	title = "EVTtitlembs_old_gods_blessing_effect.5"
	desc = "EVTDESCmbs_old_gods_blessing_effect.5"
	picture = "GFX_evt_hunting_scene"
	
	is_triggered_only = yes

	option = {
		name = "EVTOPTAmbs_old_gods_blessing_effect.5" #get lizard-lion
		
		ai_chance = {
			factor = 2
		}
		
		prestige = 15
		
		ROOT = {
			opinion = {
				modifier = opinion_grateful
				who = FROM
				years = 5
			}
		}
			
		add_trait = lizardlion
		set_character_flag = blessed_reed_siblings
	}
}
#mbs_old_gods_blessing_effect.6 - Banefort Lion
narrative_event = {
	id = mbs_old_gods_blessing_effect.6
	title = "EVTtitlembs_old_gods_blessing_effect.6"
	desc = "EVTDESCmbs_old_gods_blessing_effect.6"
	picture = "GFX_evt_hunting_scene"
	
	is_triggered_only = yes

	option = {
		name = "EVTOPTAmbs_old_gods_blessing_effect.6" #get lion
		
		ai_chance = {
			factor = 2
		}
		
		prestige = 15
		
		ROOT = {
			opinion = {
				modifier = opinion_grateful
				who = FROM
				years = 5
			}
		}
			
		add_trait = lion
		set_character_flag = blessed_lion_siblings
	}
}
