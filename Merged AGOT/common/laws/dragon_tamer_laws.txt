de_jure_laws = {
	dragon_tamer_0 = {
		show_as_title = yes
		group = dragon_tamer
		default = yes

		allow = {
			OR = {
				has_law = dragon_tamer_1
				has_law = dragon_tamer_2
			}
		}
		
		potential = {
			OR = {
				OR = {
					real_tier = KING
					real_tier = EMPEROR
				}
				AND = {
					holder_scope = {
						independent = yes
						NOT = { has_any_opinion_modifier = opinion_de_facto_liege }
					}
				}
			}
		}
		
		revoke_allowed = {
			always = no
		}
		
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0
			}
			modifier = {
				factor = 0
				holder_scope = {
					any_dynasty_member = {
						trait = dragon_rider
					}
				}
			}
		}
		
		ai_will_revoke = {
			factor = 0
		}
		
		effect = {
			hidden_tooltip = {
				revoke_law = dragon_tamer_1
				revoke_law = dragon_tamer_2
			}
		}
	}

	dragon_tamer_1 = {
		show_as_title = yes
		group = dragon_tamer

		allow = {
			OR = {
				has_law = dragon_tamer_0
				has_law = dragon_tamer_2
			}
		}
		
		potential = {
			OR = {
				OR = {
					real_tier = KING
					real_tier = EMPEROR
				}
				AND = {
					holder_scope = {
						independent = yes
						NOT = { has_any_opinion_modifier = opinion_de_facto_liege }
					}
				}
			}
		}
		
		revoke_allowed = {
			always = no
		}
		
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0.001
			}
			modifier = {
				factor = 0
				has_law = dragon_tamer_2
			}
		}
		
		ai_will_revoke = {
			factor = 0
		}
		
		effect = {
			hidden_tooltip = {
				revoke_law = dragon_tamer_0
				revoke_law = dragon_tamer_2
			}
		}
	}

	dragon_tamer_2 = {
		show_as_title = yes
		group = dragon_tamer

		allow = {
			has_law = dragon_tamer_1
		}
		
		potential = {
			OR = {
				OR = {
					real_tier = KING
					real_tier = EMPEROR
				}
				AND = {
					holder_scope = {
						independent = yes
						NOT = { has_any_opinion_modifier = opinion_de_facto_liege }
					}
				}
			}
		}
		
		revoke_allowed = {
			always = no
		}
		
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0.02
			}
		}
		
		ai_will_revoke = {
			factor = 0
		}
		
		effect = {
			hidden_tooltip = {
				revoke_law = dragon_tamer_0
				revoke_law = dragon_tamer_1
			}
		}
	}
}