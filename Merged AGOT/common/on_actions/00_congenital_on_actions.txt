#Dynastic Nicknames. Note to self; check the on_actions file when updating to ensure that the event names are still free.
#Congenital Inheritance trigger.
on_birth = {
	events = {
		congenital.0 #Congenital Traits
	}
}

on_startup = {
	events = {
		congenitalcrown.0 #Baratheon/Durrandon
		congenitalcrown.1 #Baratheon/Durrandon
		congenitalcrown.2 #Baratheon/Durrandon
		congenitalcrown.3 #Reyne/Lannister
		congenitalcrown.4 #Tully
		congenitalcrown.5 #Reyne/Lannister
		congenitalcrown.6 #Arryn
		congenitalcrown.7 #Tyrell
		congenitalcrown.8 #Dayne
		congenitalcrown.9 #Tarth
		congenitalcrown.10 #Frey
		congenitalcrown.11 #Hightower
	}
}

on_yearly_pulse = {
	events = {
		congenitalcrown.0 #Baratheon/Durrandon
		congenitalcrown.1 #Baratheon/Durrandon
		congenitalcrown.2 #Baratheon/Durrandon
		congenitalcrown.3 #Reyne/Lannister
		congenitalcrown.4 #Tully
		congenitalcrown.5 #Reyne/Lannister
		congenitalcrown.6 #Arryn
		congenitalcrown.7 #Tyrell
		congenitalcrown.8 #Dayne
		congenitalcrown.9 #Tarth
		congenitalcrown.10 #Frey
		congenitalcrown.11 #Hightower
	}
}